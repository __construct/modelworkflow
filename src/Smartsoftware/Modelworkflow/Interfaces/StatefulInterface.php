<?php namespace Smartsoftware\Modelworkflow\Interfaces;

use Smartsoftware\Modelworkflow\Workflow;

interface StatefulInterface {

    public function setObjState($new_status);

    public function getObjState();

	public function getOriginal();

	public function getModelStatus();

	/**
	 * @return Workflow
	 */
	public function getWorkflow();
}